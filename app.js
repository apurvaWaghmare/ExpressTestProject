var createError = require('http-errors');
// var express = require('express');
var path = require('path');
var fs = require('fs');
// var cookieParser = require('cookie-parser');
// var bodyParser = require('body-parser');
var logger = require('morgan');

const express = require('express');
const bodyParser = require('body-parser');
// const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const app = express();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var product = require('./routes/product')
var validator = require('express-validator');

// view engine setup
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.engine('html', function(path, opt, fn) {
    fs.readFile(path, 'utf-8', function(error, str) {
        if (error)
            return str;
        return fn(null, str);
    });
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/product', product);
// app.use('/productlist', product);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
// app.delete('/newfile.json/:id', function(req, res) {
//     if(json.length <= req.params.id) {
//       res.statusCode = 404;
//       return res.send('Error 404: No quote found');
//     }
  
//     json.splice(req.params.id, 1);
  
//   });


module.exports = app;
